import React from 'react';
import './assets/styles/common.scss';
import { Typewriter } from './components/Typewriter';

const App = () => {
    return (
        <>
            <Typewriter
                strings={['Test 1', 'Test 2', 'Test 3']}
                staticWordIndex={[0]}
                delay={50}
                loop={true}
            />
        </>
    );
};

export default App;
