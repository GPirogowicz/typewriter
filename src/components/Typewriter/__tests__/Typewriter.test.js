import React from 'react';
import { shallow, mount } from 'enzyme';
import { act } from 'react-dom/test-utils';
import Typewriter from '../Typewriter';

describe('Typewriter test (index 0 is static)', () => {
    let wrapper;
    const delay = 50;
    const strings = ['Test 1', 'Test 2', 'Test 3'];
    jest.useFakeTimers();
    beforeEach(() => {
        wrapper = mount(
            <Typewriter
                strings={strings}
                delay={delay}
                loop={true}
                staticWordIndex={[0]}
            />
        );
    });
    it('render Typewriter without crash', () => {
        expect(wrapper).not.toBeNull();
    });
    it('render empty element with class typewriter__static-lines', () => {
        expect(wrapper.find('div.typewriter__static-lines').text()).toEqual('');
    });
    it('render element with class typewriter__static-lines after delay', () => {
        act(() => {
            jest.advanceTimersByTime(delay * strings[0].length + delay * 2);
        });
        expect(wrapper.find('div.typewriter__static-lines').text()).toEqual(
            'Test 1'
        );
        act(() => {
            jest.advanceTimersByTime(delay * strings[1].length + delay);
        });
        wrapper.update();
        expect(
            wrapper.find('div.typewriter__typing-line--rotate').text()
        ).toEqual('Test 2');
    });
    jest.clearAllTimers();
});
