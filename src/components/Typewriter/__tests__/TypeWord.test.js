import React from 'react';
import { shallow, mount } from 'enzyme';
import { TypeWord } from '../index';
import { act } from 'react-dom/test-utils';

describe('TypeWord test with default props', () => {
    it('render TypeWord without crash', () => {
        const wrapper = shallow(<TypeWord />);
        expect(wrapper).not.toBeNull();
    });
    it('render empty element with class type-word', () => {
        const wrapper = shallow(<TypeWord />);
        expect(wrapper.find('span.type-word').text()).toEqual('');
    });
});

describe('TypeWord test without rotate', () => {
    let wrapper;
    const delay = 50;
    const word = 'Test TypeWord 1';
    jest.useFakeTimers();
    beforeEach(() => {
        wrapper = mount(<TypeWord word={word} delay={delay} />);
    });
    it('render TypeWord without crash', () => {
        expect(wrapper).not.toBeNull();
    });
    it('render empty element with class type-word', () => {
        expect(wrapper.find('span.type-word').text()).toEqual('');
    });
    it('render element with class type-word after delay', () => {
        act(() => {
            jest.advanceTimersByTime(delay);
        });
        expect(wrapper.find('span.type-word').text()).toEqual('T');
        act(() => {
            jest.advanceTimersByTime(delay * word.length - delay);
        });
        expect(wrapper.find('span.type-word').text()).toEqual(word);
    });
    jest.clearAllTimers();
});

describe('TypeWord test with rotate', () => {
    let wrapper;
    const delay = 50;
    const word = 'Test TypeWord 1';
    jest.useFakeTimers();
    beforeEach(() => {
        wrapper = mount(<TypeWord word={word} delay={delay} rotate={true} />);
    });
    it('render element with class type-word after delay', () => {
        act(() => {
            jest.advanceTimersByTime(delay * word.length);
        });
        expect(wrapper.find('span.type-word').text()).toEqual(word);
        act(() => {
            jest.advanceTimersByTime(delay * word.length + delay);
        });
        expect(wrapper.find('span.type-word').text()).toEqual('');
    });
    jest.clearAllTimers();
});

describe('TypeWord test with rotate and loop', () => {
    let wrapper;
    const delay = 50;
    const word = 'Test';
    jest.useFakeTimers();
    beforeEach(() => {
        wrapper = mount(
            <TypeWord word={word} delay={delay} rotate={true} loop={true} />
        );
    });
    it('render element with class type-word after delay', () => {
        act(() => {
            jest.advanceTimersByTime(delay * word.length);
        });
        expect(wrapper.find('span.type-word').text()).toEqual(word);
        act(() => {
            jest.advanceTimersByTime(delay * word.length + delay);
        });
        expect(wrapper.find('span.type-word').text()).toEqual('');
        act(() => {
            jest.advanceTimersByTime(delay * word.length + delay);
        });
        expect(wrapper.find('span.type-word').text()).toEqual(word);
    });
    jest.clearAllTimers();
});
