import React from 'react';
import PropTypes from 'prop-types';
import { useTypeWord } from './utils';

const TypeWord = ({
    word = '',
    rotate = false,
    wrapperClassName = '',
    onFinishTyping,
    delay = 100,
    loop = false,
}) => {
    const typed = useTypeWord(word, delay, onFinishTyping, rotate, loop);
    return <span className={`type-word ${wrapperClassName}`}>{typed}</span>;
};

TypeWord.propTypes = {
    word: PropTypes.string,
    rotate: PropTypes.bool,
    wrapperClassName: PropTypes.string,
    onFinishTyping: PropTypes.func,
    delay: PropTypes.number,
    loop: PropTypes.bool,
};

export default TypeWord;
