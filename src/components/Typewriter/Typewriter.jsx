import React, { useState } from 'react';
import PropTypes from 'prop-types';
import TypeWord from './TypeWord';
import './style.scss';

const Typewriter = ({
    strings = [],
    loop = false,
    staticWordIndex = [],
    delay = 100,
}) => {
    const staticLinesWords = strings.filter((string, index) =>
        staticWordIndex.includes(index)
    );
    const dynamicLineWords = strings.filter(
        (string, index) => !staticWordIndex.includes(index)
    );
    const [staticWord, setStaticWord] = useState(staticLinesWords[0] || '');
    const [dynamicWord, setDynamicWord] = useState(dynamicLineWords[0] || '');
    const [writtenWords, setWrittenWords] = useState([]);

    const onFinishTypingStaticWord = word => {
        setWrittenWords(prevState => {
            prevState.push(word);
            return [...prevState];
        });
        const wordIndex = staticLinesWords.findIndex(string => string === word);
        if (wordIndex !== -1 && staticLinesWords[wordIndex + 1]) {
            setStaticWord(staticLinesWords[wordIndex + 1]);
        }
    };
    const onFinishTypingDynamicWord = word => {
        const wordIndex = dynamicLineWords.findIndex(string => string === word);
        if (wordIndex !== -1 && dynamicLineWords[wordIndex + 1]) {
            setDynamicWord(dynamicLineWords[wordIndex + 1]);
        } else if (
            loop &&
            dynamicLineWords.length > 0 &&
            dynamicLineWords[dynamicLineWords.length - 1] === word
        ) {
            setDynamicWord(dynamicLineWords[0]);
        }
    };
    return (
        <div className={'typewriter'}>
            <div className={'typewriter__static-lines'}>
                {writtenWords.map((word, key) => (
                    <div key={key} className={'written-word'}>
                        {word}
                    </div>
                ))}
            </div>
            {writtenWords.length !== staticLinesWords.length && (
                <div className={'typewriter__typing-line'}>
                    <TypeWord
                        word={staticWord}
                        onFinishTyping={onFinishTypingStaticWord}
                        delay={delay}
                        loop={loop}
                    />
                </div>
            )}
            {writtenWords.length === staticLinesWords.length && (
                <div className={'typewriter__typing-line typewriter__typing-line--rotate'}>
                    <TypeWord
                        word={dynamicWord}
                        onFinishTyping={onFinishTypingDynamicWord}
                        delay={delay}
                        rotate
                        loop={loop}
                    />
                </div>
            )}
        </div>
    );
};

Typewriter.propTypes = {
    strings: PropTypes.array.isRequired,
    staticWordIndex: PropTypes.array,
    loop: PropTypes.bool,
    delay: PropTypes.number,
};

export default Typewriter;
