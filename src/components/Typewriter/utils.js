import { useState, useEffect } from 'react';

const useTypeWord = (word, delay, onFinishTyping, rotate, loop) => {
    const [typed, setTyped] = useState('');
    const [charIndex, setCharIndex] = useState(0);
    const [typingEnd, setTypingEnd] = useState(false);
    const clearState = () => {
        setTyped('');
        setCharIndex(0);
        setTypingEnd(false);
    };
    useEffect(() => {
        clearState();
    }, [word]);
    useEffect(() => {
        let _isMounted = true;
        let timer = setTimeout(() => {
            if (charIndex !== word.length && !typingEnd) {
                setCharIndex(charIndex + 1);
                setTyped(typed.concat(word.charAt(charIndex)));
            } else if (rotate && typingEnd && typed.length !== 0) {
                setTyped(typed.slice(0, -1));
            } else {
                setTypingEnd(true);
            }

            if (
                (typingEnd && !rotate) ||
                (typingEnd && rotate && typed.length === 0)
            ) {
                onFinishTyping && onFinishTyping(word);
                if (_isMounted && loop) clearState();
            }
        }, delay);
        return () => {
            clearTimeout(timer);
            _isMounted = false;
        };
    }, [typed, typingEnd]);

    return typed;
};

export { useTypeWord };
